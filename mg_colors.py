#!/usr/bin/env python3


class Color(object):
    """
    Parent class for the RGB and HSL colors
    """

    def __init__(self):
        pass

    @property
    def theta(self):
        """
        Returns the theta value of the color on the polar
        coordinate system color wheel. This is an alias
        for the hue property.

        Returns:
            float: The angle on the color wheel, in degrees
        """
        return self.hue
    
    @property
    def rgb(self):
        """
        Returns a tuple of the (red, green, blue) values
        of the color

        Returns:
            tuple: The color's red, green, and blue values
        """
        return self.red, self.green, self.blue

    @property
    def hsl(self):
        """
        Returns a tuple of the (hue, saturation, lightness)
        values of the color

        Returns:
            tuple: The color's hue, saturation, and lightness
        """
        return self.hue, self.saturation, self.lightness
    
    def rgb_string(self, delimiter=", "):
        """
        Converts the RGB tuple (self.rgb) to a string

        Args:
            delimiter (str): [Optional] The delimiter
                that separates the elements of the string.
                Defaults to ", ".
        """
        rgb_string_array = [str(x) for x in self.rgb]
        color_string = delimiter.join(rgb_string_array)
        return color_string
    
    def to_percent(self):
        """
        Converts the color's RGB values from a range of
        0 - 255 to a range of 0 - 100

        Returns:
            tuple: The RGB values, as a percent
        """
        r_pct = self.red/255
        g_pct = self.green/255
        b_pct = self.blue/255

        return r_pct, g_pct, b_pct

    def to_hsl(self):
        """
        Converts the color's RGB values to HSL

        Values are returned as:
            Hue: An angle, in degrees (0 to 360)
            Saturation: A percent, 0 to 100
            Lightness: A percent, 0 to 100

        Returns:
            tuple: The color's HSL values
        """
        # Change the Range from 0-255 to 0-1
        r, g, b = self.to_percent()

        c_max = max(r, g, b)
        c_min = min(r, g, b)
        delta = c_max - c_min

        # Lightness Calculation
        lightness = (c_max + c_min) / 2

        if delta == 0:
            hue = 0
            saturation = 0

        else:
            saturation = delta / (c_max + c_min) if lightness < 0.5 else delta / (2 - c_max - c_min)

            delta_r = (((c_max - r)/6) + (delta/2)) / delta
            delta_g = (((c_max - g)/6) + (delta/2)) / delta
            delta_b = (((c_max - b)/6) + (delta/2)) / delta

            if r == c_max:
                hue = delta_b - delta_g
            elif g == c_max:
                hue = (1/3) + delta_r - delta_b
            elif b == c_max:
                hue = (2/3) + delta_g - delta_r
            
            if hue < 0:
                hue += 1
            elif hue > 1:
                hue -= 1
            
            hue *= 360
            saturation *= 100
            lightness *= 100
        
        return hue, saturation, lightness
    
    @staticmethod
    def hue2rgb(v1, v2, vh):
        """
        Converts a hue to RGB
        """
        if vh < 0:
            vh += 1
        elif vh > 1:
            vh -= 1
        
        if (6 * vh) < 1:
            return (v1 + (v2 - v1) * 6 * vh)
        
        if (2 * vh) < 1:
            return v2
        
        if (3 * vh) < 2:
            return (v1 + (v2 - v1) * ((2/3 - vh) * 6))
        
        return v1

    def to_rgb(self):
        """
        Converts the color's HSL values to RGB values, which are
        returned as integers between 0 and 255.

        Returns:
            tuple: The color's red, green, and blue values
        """
        hue = self.hue/360
        saturation = self.saturation/100
        lightness = self.lightness/100

        if saturation == 0:
            red = lightness * 255
            green = lightness * 255
            blue = lightness * 255
        
        else:
            v2 = lightness * (1 + saturation) if lightness < 0.5 else (lightness + saturation) - (saturation * lightness)
            v1 = 2 * lightness - v2

            red = round(255 * self.hue2rgb(v1, v2, hue + 1/3))
            green = round(255 * self.hue2rgb(v1, v2, hue))
            blue = round(255 * self.hue2rgb(v1, v2, hue - 1/3))
        
        return red, green, blue
    
    def complimentary_color(self): #, return_hsl=False):
        """
        Return's the color's complimentary color, as an HSLColor object

        Returns:
            HSLColor: The color, as an HSLColor object
        """
        hue = self.hue + 180
        if hue >= 360:
            hue -= 360

        complimentary_color = HSLColor(hue, self.saturation, self.lightness)
        return complimentary_color
        
    def adjacent_colors(self, angle=30):
        """
        Returns the color's two adjacent colors, as HSL objects

        Params:
            angle (float): [Optional] The offset angle for the adjacent colors,
                in degrees. This should be between 20 and 60. Default is 30.
            
        Returns:
            tuple: The two adjacent colors, as HSLColor objects
        """
        h1 = self.hue + angle
        h2 = self.hue - angle

        adjacent1 = HSLColor(h1, self.saturation, self.lightness)
        adjacent2 = HSLColor(h2, self.saturation, self.lightness)

        adj_colors = (adjacent1, adjacent2)
        return adj_colors
    
    def triad_colors(self, angle=30, return_complimentary=False):
        """
        Returns the color's two triad colors, as HSL objects

        Args:
            angle (float): [Optional] The offset angle for the adjacent colors,
                in degrees. This should be between 20 and 60. Default is 30.
            return_complimentary (boolean): [Optional] Set to true to include
                the complimentary color in the returned tuple. Default is False.
                If this is set to True, the returned tuple's colors will be
                (triad1, complimentary, triad2)
            
        Returns:
            tuple: The two triad colors, as HSLColor objects
        """
        # h, s, l = complimentary_color(red, green, blue, True)
        complimentary_color = self.complimentary_color()
        h1 = complimentary_color.hue + angle
        h2 = complimentary_color.hue - angle

        triad1 = HSLColor(h1, self.saturation, self.lightness)
        triad2 = HSLColor(h2, self.saturation, self.lightness)

        triad_colours = (triad1, complimentary_color, triad2) if return_complimentary else (triad1, triad2)
        return triad_colours
    
    def tetrad_colors(self, angle=30):
        """
        Returns the color's four tetrad colors colors, as HSL objects

        Args:
            angle (float): [Optional] The offset angle for the tetrad colors,
                in degrees. This should be between 25 and 75. Default is 30.
            
        Returns:
            tuple: The three tetrad colors, as HSLColor objects (adjacent, complimentary, triad))
        """
        # Adjacent Color
        adj_h = self.hue + angle
        adjacent = HSLColor(adj_h, self.saturation, self.lightness)
        
        # Complimentary Color
        complimentary = self.complimentary_color()

        # Triad Color
        tr_h = complimentary.hue + angle
        triad = HSLColor(tr_h, self.saturation, self.lightness)

        # Assemble Tetrads
        tetrads = (adjacent, complimentary, triad)
        return tetrads
    
    def scaled_rgb(self, multiplier):
        """
        Scales an RGB color for calculating the monochromatic shades

        Args:
            multiplier (float): The multiplier by which to scale the RGB
                components. Set to negative for a darker color, and set
                to positive for a lighter color.
            
        Returns:
            RGBColor: The color, as an RGBColor object
        """
        scaled_rgb_values = []
        for component in (self.red, self.green, self.blue):
            scaled_value = component * (1 + multiplier)
            if scaled_value > 255:
                scaled_value = 255
            elif scaled_value < 0:
                scaled_value = 0
            scaled_rgb_values.append(scaled_value)
        
        scaled_color = RGBColor(*scaled_rgb_values)
        return scaled_color
    
    def monochromatic_shades(self, multiplier):
        """
        Returns the color's lighter and darker monochromatic shades, as
        RGBColor objects

        Args:
            multiplier (float): The multiplier by which to scale the RGB
                components. Because this method calculates both the light
                and dark shades, the sign does not matter.
            
        Returns:
            tuple: A tuple of RGBColor objects (light, dark)
        """
        multiplier = abs(multiplier)

        if multiplier >= 1:
            multiplier /= 100

        light_shade = self.scaled_rgb(multiplier)
        dark_shade = self.scaled_rgb(-multiplier)
        shades = (light_shade, dark_shade)
        return shades


class HSLColor(Color):

    def __init__(self, hue, saturation, lightness):
        """
        Instantiates an HSL color object.

        Args:
            hue (float): The color's hue, as an angle in degrees (0 to 360)
            saturation (float): The color's saturation, as a percentage (0 to 100)
            lightness (float): The color's lightness, as a percentage (0 to 100)
        
        Returns
            Void
        """
        self.hue = hue
        self.saturation = saturation
        self.lightness = lightness

        rgb = self.to_rgb()
        self.red = rgb[0]
        self.green = rgb[1]
        self.blue = rgb[2]


class RGBColor(Color):

    def __init__(self, red, green, blue):
        """
        Instantiates an RGB color object.

        Args:
            red (int): The red component (0 to 255)
            green (int): The green component (0 to 255)
            blue (int): The blue component (0 to 255)
        
        Returns:
            Void
        """
        self.red = red
        self.green = green
        self.blue = blue

        hsl = self.to_hsl()
        self.hue = hsl[0]
        self.saturation = hsl[1]
        self.lightness = hsl[2]