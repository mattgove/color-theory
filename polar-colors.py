#!/usr/bin/env python3
import matplotlib.pyplot as plt
import mg_colors as mgc
import numpy as np


FIGSIZE = (10, 7.5)

# color_object is an mgc.RGBColor or mgc.HSLColor object
def plot_colorline(ax, color_object, label=None):
    """
    Plots an individual color line on the polar coordinate system
    and saves it to the hard drive as a *.jpg file.

    Args:
        ax (matplotlib.pyplot): The matplotlib pylot object containing the plot
        color_object (Color): An RGBColor or HSLColor object
        label (str): [Optional] The color's label in the legend. If left blank,
            it will not add the color to the legend.
        
    Returns:
        matplotlib.pyplot: The matplotlib plot, with the new color line plotteds
    """
    rs = np.arange(0, 1, 0.01)
    theta = np.deg2rad(color_object.theta)
    legend_label = "{} Color".format(label) if label else None
    color = [color_object.to_percent()]

    for r in rs:
        ax.scatter(theta, r, s=5, c=color, zorder=3)
    ax.scatter(theta, rs[-1], c=color, zorder=3, label=legend_label)

    return ax


def plot_filename(primary_color, plot_type, delimiter="-"):
    """
    Returns the filepath for the output *.jpg image file, relative to this script

    Args:
        primary_color (RGBColor or HSLColor): The plot's primary color
        plot_type (str): The color type being plotted - i.e. adjacent, triad, tetrad
        delimiter (str): [Optional] The delimiter between words. Defaults to a dash.
    
    Returns:
        str: The output filepath, relative to the script
    """
    rgb_string = primary_color.rgb_string(delimiter)
    plot_type = plot_type.lower().replace(" ", "-")
    filename = "plots/rgb-{}-{}.jpg".format(rgb_string, plot_type)
    return filename



def plot_complimentary_colors(primary_color):
    """
    Plots the primary color and its complimentary color on a
    polar coordinate system and saves it to a *.jpg file.

    Args:
        primary_color (RGBColor or HSLColor): The primary color
    
    Returns:
        Void
    """
    fig = plt.figure()
    fig.set_size_inches(*FIGSIZE)
    ax = fig.add_subplot(111, projection="polar")

    # Primary Color
    plot_colorline(ax, primary_color, "Primary")

    # Complimentary Color
    complimentary_color = primary_color.complimentary_color()
    plot_colorline(ax, complimentary_color, "Secondary")

    ax.set_title("Complimentary Color")
    ax.legend(loc="best", bbox_to_anchor=(0.2, 0.05))

    plot_fname = plot_filename(primary_color, "Complimentary")
    plt.savefig(plot_fname)
    
    print("Successfully saved complimentary color plot to {}".format(plot_fname))
    return None


def plot_adjacent_colors(primary_color, angle=30):
    """
    Plots the primary color and its two adjacent colors on a
    polar coordinate system and saves it to a *.jpg file.

    Args:
        primary_color (RGBColor or HSLColor): The primary color on the plot
        angle (float): [Optional] The offset angle for the adjacent colors,
                in degrees. This should be between 20 and 60. Default is 30.
    
    Returns:
        Void
    """
    fig = plt.figure()
    fig.set_size_inches(*FIGSIZE)
    ax = fig.add_subplot(111, projection="polar")

    # Primary Color
    plot_colorline(ax, primary_color, "Primary")

    # Adjacent Colors
    adjacent_colors = primary_color.adjacent_colors(angle)
    for color in adjacent_colors:
        plot_colorline(ax, color, "Adjacent")

    ax.set_title("Adjacent Colors")
    ax.legend(loc="best", bbox_to_anchor=(0.08, 0.09))
    
    plot_fname = plot_filename(primary_color, "Adjacent")
    plt.savefig(plot_fname)
    
    print("Successfully saved adjacent color plot to {}".format(plot_fname))
    return None


def plot_triad_colors(primary_color, angle=30, complimentary=False):
    """
    Plots the primary color and its two triad colors on a
    polar coordinate system and saves it to a *.jpg file.

    Args:
        primary_color (RGBColor or HSLColor): The primary color on the plot
        angle (float): [Optional] The offset angle for the adjacent colors,
                in degrees. This should be between 20 and 60. Default is 30.
        complimentary (boolean): [Optional] Set to True to plot the complimentary
            color on the plot as well. Default is False.
    
    Returns:
        Void
    """
    fig = plt.figure()
    fig.set_size_inches(*FIGSIZE)
    ax = fig.add_subplot(111, projection="polar")

    # Primary Color
    plot_colorline(ax, primary_color, "Primary")

    # Triad Colors
    triad_colors = primary_color.triad_colors(angle, complimentary)
    color_index = 0
    for color in triad_colors:
        label = "Triad" if complimentary and color_index %2 == 0 else "Complimentary"
        plot_colorline(ax, color, label)
        color_index += 1
    
    bbox = (0.18, 0.12) if complimentary else (0.08, 0.09)
    
    ax.set_title("Triad Colors")
    ax.legend(loc="best", bbox_to_anchor=bbox)
    
    plot_fname = plot_filename(primary_color, "Triad")
    plt.savefig(plot_fname)
    
    print("Successfully saved triad color plot to {}".format(plot_fname))
    return None


def plot_tetrad_colors(primary_color, angle=30):
    """
    Plots the primary color and its three tetrad colors on a
    polar coordinate system and saves it to a *.jpg file.

    Args:
        primary_color (RGBColor or HSLColor): The primary color on the plot
        angle (float): [Optional] The offset angle for the adjacent colors,
                in degrees. This should be between 20 and 60. Default is 30.
    
    Returns:
        Void
    """
    fig = plt.figure()
    fig.set_size_inches(*FIGSIZE)
    ax = fig.add_subplot(111, projection="polar")

    # Primary Color
    plot_colorline(ax, primary_color, "Primary")

    # Tetrad Colors
    tetrad_colors = primary_color.tetrad_colors(angle)
    for color in tetrad_colors:
        plot_colorline(ax, color, "Tetrad")
    
    ax.set_title("Tetrad Colors")
    ax.legend(loc="best", bbox_to_anchor=(0.03, 0.12))
    
    plot_fname = plot_filename(primary_color, "Tetrad")
    plt.savefig(plot_fname)
    
    print("Successfully saved tetrad color plot to {}".format(plot_fname))
    return None


def plot_monochromatic_shades(primary_color, multiplier):
    """
    Plots the primary color and its light/dark monochromatic shades on a
    polar coordinate system and saves it to a *.jpg file.

    Args:
        primary_color (RGBColor or HSLColor): The primary color on the plot
        multiplier (float): The multiplier by which to scale the RGB
                components.
    
    Returns:
        Void
    """
    # Set Multiplier Label for Title
    multiplier_label = str(multiplier) if multiplier >= 1 else str(multiplier * 100)
    rgb_string = primary_color.rgb_string()
    title = "Monochromatic Shades: ±{}%\nRGB({})".format(multiplier_label, rgb_string)

    # Get Shades
    shades = primary_color.monochromatic_shades(multiplier)

    # Colors
    colors = [primary_color.to_percent()]

    for color in shades:
        pct = color.to_percent()
        colors.append(pct)

    fig = plt.figure()
    fig.set_size_inches(*FIGSIZE)
    ax = fig.add_subplot(111)

    labels = ["Primary", "Light", "Dark"]
    values = [1, 1, 1]

    ax.bar(labels, values, color=colors)
    ax.set_title(title)
    
    plot_fname = plot_filename(primary_color, "Monochomatic Shading")
    plt.savefig(plot_fname)
    
    print("Successfully saved monochromatic shading color plot to {}".format(plot_fname))
    return None





if __name__ == "__main__":
    ANGLE = 45

    primary_color = mgc.RGBColor(51, 102, 204)
    plot_complimentary_colors(primary_color)
    plot_adjacent_colors(primary_color, ANGLE)
    plot_triad_colors(primary_color, ANGLE)
    plot_tetrad_colors(primary_color, ANGLE)
    plot_monochromatic_shades(primary_color, 40)